package com.devcamp.restapicustomeraccount.Controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapicustomeraccount.Controller.models.Account;
import com.devcamp.restapicustomeraccount.Controller.models.Customer;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CustomerAccountAPI {
  @GetMapping("/accounts")
  public ArrayList<Account> getAccountList() {
    Customer customer1 = new Customer(1, "Teo", 30);
    Customer customer2 = new Customer(2, "Bich", 25);
    Customer customer3 = new Customer(3, "Chinh", 20);
    System.out.println(customer1.toString());
    System.out.println(customer2.toString());
    System.out.println(customer3.toString());

    Account account1 = new Account(1, customer1, 200000);
    Account account2 = new Account(2, customer2, 180000);
    Account account3 = new Account(3, customer3, 170000);
    System.out.println(account1.toString());
    System.out.println(account2.toString());
    System.out.println(account3.toString());

    ArrayList<Account> accounts = new ArrayList<>();
    accounts.add(account1);
    accounts.add(account2);
    accounts.add(account3);

    return accounts;
  }
}
